import React from 'react';
import { TabNavigator, StackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';

import TopicsFlatList from './TopicsFlatList';
import CategoryTab from './CategoryTab';

export const Tabs = TabNavigator({
  all: {
    screen: TopicsFlatList,
    navigationOptions: {
      tabBarLabel: 'Feed',
      tabBarIcon: ({ tintColor }) => <Icon name="list" size={35} color={tintColor} />,
    },
  },
  nature: {
    screen: TopicsFlatList,
    navigationOptions: {
      tabBarLabel: 'Nature',
      tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor} />
    },
  },
  animals: {
    screen: TopicsFlatList,
    navigationOptions: {
      tabBarLabel: 'Animal',
      tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor} />
    },
  },
  art: {
    screen: TopicsFlatList,
    navigationOptions: {
      tabBarLabel: 'Art',
      tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor} />
    },
  },
},{
  'lazy': true,
});


