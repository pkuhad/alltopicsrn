import React, { Component } from "react";
import { View, Text, FlatList, ActivityIndicator, Image } from "react-native";
import { List, ListItem } from "react-native-elements";

export default class TopicsFlatList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      seed: 1,
      error: null,
      refreshing: false
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {
    const { max_id } = this.state;
    category = this.props.navigation.state.key;

    url = 'http://sid.alltopics.com/apiv2/posts/?'
    params = {}
    params['content_type'] = 'article';
    if (max_id) {
      params['max_id'] = max_id;
    }
    if (category && category !== 'all') {
      params['category'] = category;
    }
    console.log(params);
    var esc = encodeURIComponent;
    var query = Object.keys(params)
        .map(k => esc(k) + '=' + esc(params[k]))
        .join('&');
    console.log('query constructed ', query)
    url = url + query;
    console.log('constructed url ', url)

    this.setState({ loading: true });

    fetch(url, params)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: max_id === undefined ? res : [...this.state.data, ...res],
          error: res.error || null,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  handleLoadMore = () => {
    len = this.state.data.length,
    last_item = this.state.data[len-1],
    val_max_id = last_item.id,
    console.log(val_max_id),
    this.setState(
      {
        max_id: val_max_id
      },

      () => {
        this.makeRemoteRequest();
      }
    );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
      <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
            <ListItem
              avatarStyle={{ width: 150, height:150 }}
              title={item.title}
              containerStyle={{ borderBottomWidth: 0 }}
              avatar={{ uri: item.post_thumbnail }}
            />
          )}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={this.renderSeparator}
          ListFooterComponent={this.renderFooter}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={50}
        />
      </List>
    );
  }
}